package com.epam.training.factories;

import com.epam.training.implementations.BookGetImplDB;
import com.epam.training.implementations.BookGetImplRAM;
import com.epam.training.interfaces.IBookDAO;

/**
 * Factory for the choice of implementation to the database connection
 */
public enum EnumDAOFactory {
    BOOK_DB_DAO {
        @Override
        public IBookDAO getDAO() {
            return new BookGetImplDB();
        }
    },

    BOOK_HARD_DAO {
        @Override
        public IBookDAO getDAO() {
            return new BookGetImplRAM();
        }
    };


    /**
     * @return implementing DB class
     */
    public abstract IBookDAO getDAO();

}
