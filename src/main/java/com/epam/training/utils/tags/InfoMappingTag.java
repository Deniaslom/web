package com.epam.training.utils.tags;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.*;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.tagext.TagSupport;

public class InfoMappingTag extends TagSupport {
    private static final Logger LOGGER = Logger.getLogger(InfoMappingTag.class.getName());
    private List<String> urls = new ArrayList();
    private List<String> resources = new ArrayList();
    private int count = 0;

    @Override
    public int doStartTag() {
        Map<String, ? extends ServletRegistration> servletRegistrations = pageContext.getServletContext().getServletRegistrations();
        Set<String> listNamesServlet = servletRegistrations.keySet();

        for (String name : listNamesServlet) {
            ServletRegistration servletRegistration = pageContext.getServletContext().getServletRegistration(name);
            urls.add(String.valueOf(servletRegistration.getMappings()));
            resources.add(servletRegistration.getName());
        }
        
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() {

        if (urls.size() > count) {
            try {
                pageContext.getRequest().setAttribute("url", urls.get(count));
                pageContext.getRequest().setAttribute("resource", resources.get(count));
                pageContext.getOut().append("<br>");
                count++;
            } catch (IOException e) {
                LOGGER.error("operation error in info class InfoMappingTag", e);
            }
            return EVAL_BODY_AGAIN;
        }
        return SKIP_BODY;
    }

}

