package com.epam.training.implementations;

import com.epam.training.interfaces.IBookDAO;
import com.epam.training.beans.Book;

import java.util.ArrayList;
import java.util.List;



/**
 * This class is a DATABASE located in RAM
 */
public class BookGetImplRAM implements IBookDAO {
    private static ArrayList<Book> listBook = new ArrayList<>();

    public BookGetImplRAM() {
        bookAdd();
    }

    /**
     * starting list of books in RAM
     */
    private static void bookAdd(){
        listBook.add(new Book(0,"Iron sky 1", "Jesika Stoyne", "1text text text", 5453));
        listBook.add(new Book(1,"Iron sky 2", "Bagira hard", "2text text text", 7653));
        listBook.add(new Book(2,"Iron sky 3", "Joe easy", "3text text text", 8753));
        listBook.add(new Book(3,"Iron sky 4", "Maks", "4text text text", 11153));
    }

    /**
     * @return list of books from ram
     */
    @Override
    public List<Book> getBooks() {
        return listBook;
    }

    @Override
    public void addBook(String name, String author, String description, int cost){
        listBook.add(new Book(listBook.size()+1, name, author, description, cost));
    }

    @Override
    public void updateBook(int id, String author, String description){
        Book bookDeleted = listBook.remove(id);
        listBook.add(id, new Book(id, bookDeleted.getName(), author, description, bookDeleted.getCost()));
    }

    @Override
    public Book getBookById(int id) {
        return listBook.get(id);
    }


}
