
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>

  <form action="bookUpdate" method="post">
    <input name="bookId" value="${bookUpdate.id}">
    <input type="text" name="author" placeholder="${bookUpdate.author}"/>
    <input type="text" name = "description" placeholder="${bookUpdate.description}"/>
    <input type="submit"/>
  </form>

  </body>
</html>
