package com.epam.training.servlets;

import com.epam.training.exceptions.DaoException;
import com.epam.training.factories.EnumDAOFactory;
import com.epam.training.interfaces.IBookDAO;
import com.epam.training.beans.Book;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@WebServlet(
        urlPatterns = { "/bookGetList" },
        initParams = {
                @WebInitParam(name = "flag", value = "false"),
                @WebInitParam(name = "seconds", value = "1")
        })

public class BookGetListServlet extends BaseController {
    private static AtomicBoolean flag = new AtomicBoolean(false);
    private static final Logger log = Logger.getLogger(BookGetListServlet.class.getName());
    private static final String BOOK_DB_DAO = "BOOK_DAO";
    private static IBookDAO bookDAO;

    /**
     * In this method, the connection to the database
     * is established by the application configuration
     */
    @Override
    public void init() throws ServletException {
        super.init();
        String dataSourceSelection = getServletConfig().getServletContext().getInitParameter(BOOK_DB_DAO);
        log.info(dataSourceSelection);
        bookDAO = EnumDAOFactory.valueOf(dataSourceSelection).getDAO();
    }

    /**
     * This method checks for the availability of the servlet,
     * retrieves the list of books from the database,
     * and transfers the list of books to index.jsp.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String stringFlag = getServletConfig().getInitParameter("flag");
        int sec = Integer.parseInt(getServletConfig().getInitParameter("seconds"));
        if(!flag.get() && stringFlag.equals("false")){
            flag.getAndSet(true);
            throw new UnavailableException("testing application", sec);
        }
        try {
            List<Book> listBook = bookDAO.getBooks();
            req.setAttribute("listBook", listBook);
        } catch (DaoException e) {
            log.log(Level.ERROR, "Database connection has problems: ", e);
        }

        forward("/index.jsp", req, resp);
    }
}
