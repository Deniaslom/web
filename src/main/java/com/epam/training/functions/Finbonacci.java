package com.epam.training.functions;

public class Finbonacci {
    public static int calculateFinbonacci(int n){
        int x = 1;
        int y = 0;
        for (int i = 0; i < n; i++)
        {
            x += y;
            y = x - y;
        }
        return y;
    }
}
