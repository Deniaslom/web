package com.epam.training.beans;

import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class XSSRequestWrapperTest {

    @Test
    public void xssProtectionScriptTag() {
        HttpServletRequest servletRequest = mock(HttpServletRequest.class);
        String parameter = "title";


        final Map<String, String> testData = new HashMap<>();
        testData.put("<script>", "&lt;script&gt;");
        testData.put("</", "&lt;/");

        testData.forEach((value, expectedValue) -> {
            when(servletRequest.getParameter(parameter)).thenReturn(value);
            String title = new XSSRequestWrapper(servletRequest).getParameter(parameter);

            assertEquals(expectedValue, title);
        });
    }

    @Test
    public void xssReturnNull() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        XSSRequestWrapper xssRequestWrapper = new XSSRequestWrapper(request);
        String parameter = "title";

        String title = xssRequestWrapper.getParameter(parameter);
        assertNull(title);
    }

    @Test
    public void bookGet() {
        System.out.println(File.separator);
    }


}