package com.epam.training.servlets.listeners;


import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;
import java.util.Map;
import java.util.Set;

@WebListener
public class InfoApplicationListener implements ServletContextListener {
    private  static final Logger log = Logger.getLogger(InfoApplicationListener.class.getName());


    /**
     * this method transfers information about the application to the console
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        Map<String, ? extends ServletRegistration> servletRegistrations = servletContextEvent.getServletContext().getServletRegistrations();
        Set<String> listNamesServlet = servletRegistrations.keySet();
        log.info("------INFORMATION SERVER------");
        String webAplication = servletContextEvent.getServletContext().getServletContextName();
        log.info(webAplication);
        for (String name: listNamesServlet) {
            log.info(name);
        }
    }

    /**
     * nothing should be cleaned
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
