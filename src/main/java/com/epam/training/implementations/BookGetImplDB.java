package com.epam.training.implementations;

import com.epam.training.exceptions.DaoException;
import com.epam.training.interfaces.IBookDAO;
import com.epam.training.beans.Book;
import com.epam.training.utils.ConnectionPool;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookGetImplDB implements IBookDAO{
	private static final String SELECT_FROM_BOOKS = "SELECT * FROM books";
	private static final Logger log = Logger.getLogger(BookGetImplDB.class.getName());

	@Override
	public List<Book> getBooks() throws DaoException {
		List<Book> listBook = new ArrayList<>();
		try(Connection cn = ConnectionPool.getInstance().getConnection();
			Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(SELECT_FROM_BOOKS)) {
			while (rs.next()) {
				int id = rs.getInt(1	);
				String name = rs.getString(2);
				String author = rs.getString(3);
				String description = rs.getString(4);
				int cost = rs.getInt(5);
				listBook.add(new Book(id, name, author, description, cost));
			}
		} catch (SQLException e) {
			log.log(Level.ERROR, "Database connection has problems in method getBooks(): ", e);
			throw new DaoException(e.getMessage());
		}
		return listBook;
	}

	@Override
	public void addBook(String name, String author, String description, int cost) throws DaoException {
		try(Connection cn = ConnectionPool.getInstance().getConnection()){
			try(PreparedStatement ps = cn.prepareStatement("INSERT INTO books (name, author, description, cost) VALUES (?, ?, ?, ?)")){
				ps.setString(1, name);
				ps.setString(2, author);
				ps.setString(3, description);
				ps.setInt(4, cost);
				ps.execute();
			}
		} catch (SQLException e) {
			log.log(Level.ERROR, "Database connection has problems in method addBook(): ", e);
			throw new DaoException(e.getMessage());
		}

	}

	@Override
	public void updateBook(int id, String author, String description) throws DaoException {
		try(Connection cn = ConnectionPool.getInstance().getConnection();
			PreparedStatement ps = cn.prepareStatement("UPDATE books SET author=?, description=? WHERE id=?")) {
			ps.setString(1, author);
			ps.setString(2, description);
			ps.setInt(3, id);
			ps.execute();
		} catch (SQLException e) {
			log.log(Level.ERROR, "Database connection has problems in method updateBook(): ", e);
			throw new DaoException(e.getMessage());
		}

	}

	@Override
	public Book getBookById(int id) throws DaoException {
		Book book = null;
		try(Connection cn = ConnectionPool.getInstance().getConnection();
			PreparedStatement pr = cn.prepareStatement("SELECT * FROM books WHERE id = ?");){
			pr.setInt(1, id);
			ResultSet rs = pr.executeQuery();

			while (rs.next()) {
				int idBook = rs.getInt(1);
				String nameBook = rs.getString(2);
				String author = rs.getString(3);
				String descrip = rs.getString(4);
				int cost = rs.getInt(5);
				book = new Book(idBook, nameBook, author, descrip, cost);
			}

			rs.close();

		} catch (SQLException e) {
			log.log(Level.ERROR, "Database connection has problems in method getBookById(): ", e);
			throw new DaoException(e.getMessage());
		}

		return book;
	}


}
