package com.epam.training.beans;


import java.util.Objects;

public class Book {

	private int id;
	private String name;
	private String author;
	private String description;
	private int cost;

	public Book() {
		super();
	}

	public Book(int id, String name, String author, String description, int cost) {
		super();
		this.id = id;
		this.name = name;
		this.author = author;
		this.description = description;
		this.cost = cost;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Book book = (Book) o;
		return cost == book.cost &&
				name.equals(book.name) &&
				author.equals(book.author);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, author, cost);
	}

	@Override
	public String toString() {
		return getName() + ";" +getAuthor() +";" +getDescription() +";"+ getCost();
	}
}
