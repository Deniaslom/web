package com.epam.training.functions;


import org.junit.Test;

public class FinbonacciTest {

    @Test
    public void calculateFinbonacci(){
        int position9 = new Finbonacci().calculateFinbonacci(9);
        assert(34 == position9);
        int position5 = new Finbonacci().calculateFinbonacci(5);
        assert(5 == position5);
        int position1 = new Finbonacci().calculateFinbonacci(1);
        assert(1 == position1);
        int position12 = new Finbonacci().calculateFinbonacci(12);
        assert(144 == position12);
        int position3 = new Finbonacci().calculateFinbonacci(3);
        assert(2 == position3);
    }

}
