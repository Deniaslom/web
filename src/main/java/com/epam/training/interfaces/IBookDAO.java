package com.epam.training.interfaces;


import com.epam.training.exceptions.DaoException;
import com.epam.training.beans.Book;

import java.util.List;

/**
 *		implementation of this interface serves as a template for creating a database connection
 *
 *		Method getBooks() returns a list of books from the database
 * 		Method addBook() adds a book to the database
 * 		Method updateBook() changes the author and description in the book by id
 * 		Method getBookById() returns a book by id
 */
public interface IBookDAO {
	List<Book> getBooks() throws DaoException;
	void addBook(String name, String author, String description, int cost) throws DaoException;
	void updateBook(int id, String author, String description) throws DaoException;
	Book getBookById(int id) throws DaoException;
}
