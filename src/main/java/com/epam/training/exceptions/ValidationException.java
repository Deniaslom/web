package com.epam.training.exceptions;

public class ValidationException extends Exception {

    private static final long serialVersionUID = 1L;

    public ValidationException(Throwable arg0) {
        super(arg0);
    }

    public ValidationException(String message) {
        super(message);
    }
}