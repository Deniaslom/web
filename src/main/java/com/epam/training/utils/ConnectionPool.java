package com.epam.training.utils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ConnectionPool {

    private  static final Logger log = Logger.getLogger(ConnectionPool.class.getName());

    private ConnectionPool(){
    }

    private static ConnectionPool instance = null;

    public static ConnectionPool getInstance(){
        if (instance == null)
            instance = new ConnectionPool();
        return instance;
    }

    public static Connection getConnection(){
        Context ctx;
        Connection c = null;
        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/PoolName");
            c = ds.getConnection();

        } catch (SQLException | NamingException e) {
            log.log(Level.ERROR,"Exceptoin from ConnectionPool" , e);
        }
        return c;
    }

}
