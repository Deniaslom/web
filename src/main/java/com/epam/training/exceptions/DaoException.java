package com.epam.training.exceptions;

public class DaoException extends Exception {

    private static final long serialVersionUID = 1L;

    public DaoException(Throwable arg0) {
        super(arg0);
    }

    public DaoException(String message) {
        super(message);
    }
}
