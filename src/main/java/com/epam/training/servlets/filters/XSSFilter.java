package com.epam.training.servlets.filters;

import com.epam.training.beans.XSSRequestWrapper;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(urlPatterns="/*")
public class XSSFilter implements Filter {

    /**
     * parameter initialization is not needed
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        filterChain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), response);
    }

    /**
     * nothing should be cleaned
     */
    @Override
    public void destroy() {

    }
}
