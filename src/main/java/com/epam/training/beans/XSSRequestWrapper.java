package com.epam.training.beans;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class XSSRequestWrapper extends HttpServletRequestWrapper {
    public XSSRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getParameter(String parameter) {
        String value = super.getParameter(parameter);
        if(value != null){
            return   value.replaceAll("<","&lt;")
                     .replaceAll(">", "&gt;");
        }
        return value;
    }

}
