package com.epam.training.servlets;

import com.epam.training.exceptions.DaoException;
import com.epam.training.exceptions.ValidationException;
import com.epam.training.factories.EnumDAOFactory;
import com.epam.training.interfaces.IBookDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import static java.lang.Integer.parseInt;

@WebServlet(urlPatterns = {"/bookAdd"})
public class AddBookServlet extends BaseController {
    private static final String BOOK_DB_DAO = "BOOK_DAO";
    private static final Logger log = Logger.getLogger(AddBookServlet.class.getName());
    private static IBookDAO bookDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        String dataSourceSelection = getServletConfig().getServletContext().getInitParameter(BOOK_DB_DAO);
        log.info(dataSourceSelection);
        bookDAO = EnumDAOFactory.valueOf(dataSourceSelection).getDAO();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String name = request.getParameter("name");
            String author = request.getParameter("author");
            String description = request.getParameter("description");
            int cost = parseInt(request.getParameter("cost"));

            checkInput(name, author, description);
            checkInputInt(cost);
            bookDAO.addBook(name, author, description, cost);
            response.sendRedirect(request.getContextPath() + "/index.jsp");

        } catch (ValidationException e) {
            forwardError(e.getMessage(), "/index.jsp", request, response);
            log.log(Level.ERROR, "Error: Data is not define: ", e);
        } catch (DaoException e) {
            log.log(Level.ERROR, "Database connection has problems: ", e);
        }
    }
}
