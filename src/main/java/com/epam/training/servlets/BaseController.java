package com.epam.training.servlets;

import com.epam.training.exceptions.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BaseController extends HttpServlet {
    protected static void checkInput(String...args) throws ValidationException {
        for(String arg : args) {
            if(arg==null||"".equals(arg.trim())) {
                throw new ValidationException("Error: Data is not define");
            }
        }
    }

    protected static void checkInputInt(int...args) throws ValidationException {
        for(int arg : args) {
            if(!(arg <= 0)) {
                throw new ValidationException("Error: Data is not define");
            }
        }
    }


    protected void forward(String url, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher(url).forward(req, res);
    }

    protected void forwardError(String cause, String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("Error", cause);
        forward(url, request, response);
    }
}
