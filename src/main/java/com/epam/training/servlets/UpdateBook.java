package com.epam.training.servlets;

import com.epam.training.exceptions.DaoException;
import com.epam.training.exceptions.ValidationException;
import com.epam.training.factories.EnumDAOFactory;
import com.epam.training.interfaces.IBookDAO;
import com.epam.training.beans.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


@WebServlet( urlPatterns = "/bookUpdate")
public class UpdateBook extends BaseController {
    private static final Logger log = Logger.getLogger(UpdateBook.class.getName());
    private static final String BOOK_DB_DAO = "BOOK_DAO";
    private static IBookDAO bookDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        String dataSourceSelection = getServletConfig().getServletContext().getInitParameter(BOOK_DB_DAO);
        log.info(dataSourceSelection);
        bookDAO = EnumDAOFactory.valueOf(dataSourceSelection).getDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bookUpdateName = req.getParameter("bookUpdate");

        try {
            Book bookUpdate = bookDAO.getBookById(Integer.parseInt(bookUpdateName));
            req.setAttribute("bookUpdate", bookUpdate);

        } catch (DaoException e) {
            log.log(Level.ERROR, "Database connection has problems: ", e);
        }
        req.getRequestDispatcher("/bookUpdate.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int bookId = Integer.parseInt(req.getParameter("bookId"));
        String author = req.getParameter("author");
        String description = req.getParameter("description");
        try {
            checkInput(author, description);
            bookDAO.updateBook(bookId, author, description);
        } catch (ValidationException e) {
            log.log(Level.ERROR, "ValidationException: ", e);
            forwardError(e.getMessage(),"/index.jsp", req, resp);
        } catch (DaoException e) {
            log.log(Level.ERROR, "Database connection has problems: ", e);
        }
        forward("/index.jsp", req, resp);
    }
}
